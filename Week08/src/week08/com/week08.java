
/**
 *   A Java program that simulates a print server
 *   queue for five different printers.
 *   The program starts three different threads,
 *   and generates a random number to sleep the
 *   threads at random intervals simulating the time
 *   it takes to print a page.
 *
 *   The printers then simulate printing 10 pages each.
 *   The status and results are written to the console,
 *   and the program notifies the user when the print job
 *   is completed.
 *
 *  Author: Dainon Snow
 *  CIT360
 *  1/28/2022
 */

package week08.com;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class week08 {

    //PSVM to define threads to execute, identifies printers, and executes the print service.
    //Some code from Brother Tuckett
    public static void main(String[] args) {

        ExecutorService printServe = Executors.newFixedThreadPool(3);

        printJob prn1 = new printJob("Sharp MX3050          ");
        printJob prn2 = new printJob("HP LaserJetPro 4200   ");
        printJob prn3 = new printJob("LexMark CX431         ");
        printJob prn4 = new printJob("Ricoh MFP 8020        ");
        printJob prn5 = new printJob("Konica C458           ");

        printServe.execute(prn1);
        printServe.execute(prn2);
        printServe.execute(prn3);
        printServe.execute(prn4);
        printServe.execute(prn5);

        printServe.shutdown();
    }
}

// Super constructor allocates a new Thread object. -- some code from docs.oracle.com tutorials
 class printJob extends Thread implements Runnable {
    public printJob(String print) {
        super(print);
    }

    //Runnable that generates sleep number and prints job queue status -- some code from crunchify.com
    public void run() {
        for (int counter = 1; counter <= 10; counter++) {
            System.out.println(getName() + " Print Job Thread ID: " + getId() + "       Page " + counter);

            //Check for Thread interrupted exception
            try {
                // Generates random number to sleep thread simulating time to print a page
                Thread.sleep((int) (Math.random() * 4000));

            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
        System.out.println("\n<== Print Job Finished for: " + getName() + "==>" + "\n");
    }
  }

