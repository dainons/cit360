package entity.template;

import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// Some code from Brother Tuckett tutorial
@WebServlet(name = "cubeServlet", urlPatterns = {"/cube-servlet"} //urlPatterns = {"/submitLogin"}

)
public class HelloServlet extends HttpServlet {

    //Method to start Tomcat and get the parameters from the index page
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String inputYear = request.getParameter("Year");
        int year = Integer.parseInt(inputYear);

        String inputArtist = request.getParameter("Artist");


        String inputAlbum = request.getParameter("Album");


        String uom = request.getParameter("uom");


        //Write the output to a blank page
        PrintWriter writer = response.getWriter();
        writer.println("<html>" + year + "&nbsp;&nbsp;&nbsp;&nbsp;" + "&nbsp;&nbsp;&nbsp;&nbsp;" + inputArtist + "&nbsp;&nbsp;&nbsp;&nbsp;" + inputAlbum + "&nbsp;&nbsp;&nbsp;&nbsp; Stars: " + uom + "</html>");
        //writer.flush();

        /*QueryDB con = QueryDB.getInstance();

        List<AlbumsEntity> artistName = con.getAlbums();*/


        AlbumsEntity artistName = new AlbumsEntity();


        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        artistName.setArtist(inputArtist);
        artistName.setAlbum(inputAlbum);
        artistName.setYear(year);
        artistName.setStars(uom);





        //save the user in the database
        session.save(artistName);


        //commit the transaction
        session.getTransaction().commit();
        //TODO Possible issue**
        session.close();

    }

    //Destroy the servlet
    public void destroy() {
    }
}

