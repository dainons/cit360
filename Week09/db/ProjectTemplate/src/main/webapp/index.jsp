<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>Album Catalog</title>
</head>
<body>
<h1>Alphabetized inventory of albums (Template)</h1>

<! some form elements from codejava.net>

<form action="cube-servlet" method="post">
  Year: <label>
  <input type="number" size="1" min="1900" max="2022" step="1" name="Year" placeholder="YYYY" required/>
</label>
  &nbsp;&nbsp;
  Artist: <label>
  <input size="15" name="Artist" placeholder="ex. Journey" required/>
</label>
  &nbsp;&nbsp;
  Album: <label>
  <input size="15" name="Album" placeholder="ex. Captured" required/>
</label>
  &nbsp;&nbsp;

  <! menu pull down from W3schools.com>

  <label for="uom">Stars:</label>
  <select name="uom" id="uom">
    <option value="one">1</option>
    <option value="two">2</option>
    <option value="three">3</option>
    <option value="four">4</option>
    <option value="five">5</option>
  </select>

  <br/><br/>
  <input type="submit" value="Enter" />
</form>
</body>
</html>