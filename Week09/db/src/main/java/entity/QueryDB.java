package entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

/** QueryDB implemented using a singleton pattern.
 *  Used to get vehicle data from the SQL Server database.
 *  This class code from Brother Tuckett.
 *  */
public class QueryDB {

    SessionFactory factory = null;
    Session session = null;

    private static QueryDB single_instance = null;

    private QueryDB()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static QueryDB getInstance()
    {
        if (single_instance == null) {
            single_instance = new QueryDB();
        }

        return single_instance;
    }

    /** Used to get more than one vehicle from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<AlbumsEntity> getAlbums() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.AlbumsEntity";
            List<AlbumsEntity> cs = (List<AlbumsEntity>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single vehicle from database */
    public AlbumsEntity getAlbum(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.AlbumsEntity where id=" + Integer.toString(id);
            AlbumsEntity c = (AlbumsEntity) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}

