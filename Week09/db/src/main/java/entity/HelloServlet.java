package entity;

import org.hibernate.Session;

import java.io.*;
import java.util.List;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

// Some code from Brother Tuckett tutorial
@WebServlet(name = "cubeServlet", urlPatterns = {"/cube-servlet"} //urlPatterns = {"/submitLogin"}

)
public class HelloServlet extends HttpServlet {

    //Method to start Tomcat and get the parameters from the index page
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String inputYear = request.getParameter("Year");
        int year = Integer.parseInt(inputYear);

        String inputArtist = request.getParameter("Artist");


        String inputAlbum = request.getParameter("Album");


        String uom = request.getParameter("uom");


        //Write the output to a blank page
        PrintWriter writer = response.getWriter();
        writer.println("<html>" + year + "&nbsp;&nbsp;&nbsp;&nbsp;" + "&nbsp;&nbsp;&nbsp;&nbsp;" + inputArtist + "&nbsp;&nbsp;&nbsp;&nbsp;" + inputAlbum + "&nbsp;&nbsp;&nbsp;&nbsp; Stars: " + uom + "</html>");
        writer.flush();

        //to get info from the database
        QueryDB con = QueryDB.getInstance();

        List<AlbumsEntity> artistName = con.getAlbums();

        //HTML BODY

        for (int i =0; i<artistName.size(); i++) {
        artistName.get(i).getArtist();
        writer.println("<html>"+artistName.get(i).getYear()+"</html>");
        writer.println("<html>"+artistName.get(i).getArtist()+"</html>");
        writer.println("<html>"+artistName.get(i).getAlbum()+"</html>");
        writer.println("<html>"+artistName.get(i).getStars()+"</html>");
        }
        //HTML END OF BODY

        AlbumsEntity artistName2 = new AlbumsEntity();


        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        artistName2.setArtist(inputArtist);
        artistName2.setAlbum(inputAlbum);
        artistName2.setYear(year);
        artistName2.setStars(uom);

        //save the user in the database
        session.save(artistName);


        //commit the transaction
        session.getTransaction().commit();
        //TODO Possible issue**
        session.close();

    }

    //Destroy the servlet
    public void destroy() {
    }
}

