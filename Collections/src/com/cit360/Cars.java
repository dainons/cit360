package com.cit360;

//Borrowed this code from Brother Tuckett
public class Cars {

    private String manufacturer;
    private String model;

    public Cars(String manufacturer, String model) {
        this.manufacturer = manufacturer;
        this.model = model;
    }

    public String toString() {
        return "Manufacturer: " + manufacturer + " Model: " + model;
    }
}
