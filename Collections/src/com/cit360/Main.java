package com.cit360;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("--- List ---");

        //Create an array list of 10 numbers
        List<Integer> Numbers = new ArrayList<>();
        Numbers.add(6);
        Numbers.add(1);
        Numbers.add(0);
        Numbers.add(6);
        Numbers.add(2);
        Numbers.add(4);
        Numbers.add(3);
        Numbers.add(5);
        Numbers.add(5);
        Numbers.add(7);

        //Error handling for empty list
        boolean lans = Numbers.isEmpty();
        if (lans){
            System.out.println("Error: List is empty");
            System.exit(1);
        }

        else

        //Sort list in ascending order with duplicates
        Collections.sort(Numbers);

        //Output sorted list to console with duplicates
        for (Integer c : Numbers) {
            System.out.print(c + " \n");
        }


        System.out.println("\n--- Queue ---");

        //Create a queue of 8 colors
        Queue<String> Queue = new LinkedList<>();
        Queue.add("Red");
        Queue.add("Yellow");
        Queue.add("Orange");
        Queue.add("Green");
        Queue.add("Blue");
        Queue.add("Purple");
        Queue.add("White");
        Queue.add("Black");

        //Error Handling for empty Queue
        boolean qans = Queue.isEmpty();
        if (qans){
            System.out.println("Error: Queue is empty");
            System.exit(1);}

        else

        //Output original queue to the console
        for (String q : Queue) {
            System.out.print(q + " \n");
        }

        //Remove head of queue
        String deleted = Queue.remove();
        System.out.println("\nRemoved head of queue "
                + deleted + " and added pink.\n");

        //Add pink to queue
        Queue.add("Pink");

        //Output new queue to console
        for (String q : Queue) {
            System.out.print(q + " \n");
        }

        System.out.println("\n--- Map ---");

        //Create map of 8 numbered pairs of colors
        Map<Integer, java.lang.constant.Constable> map = new HashMap<>();

        map.put(1, "Red");
        map.put(2, "Yellow");
        map.put(3, "Orange");
        map.put(4, "Green");
        map.put(5, "Blue");
        map.put(6, "Purple");
        map.put(7, "White");
        map.put(8, "Black");

        /*Convert map to set to print in columns
          IntelJ didn't like the code I put in here, so when I took the recommendation
          on the orange bars to the right, it changed it to this?
         */
        Set<Map.Entry<Integer, java.lang.constant.Constable>> s = map.entrySet();

        //Error handling for empty map
        boolean mans = map.isEmpty();
        if (mans){
            System.out.println("Error: List is empty");
            System.exit(1);
        }

        else

        //Output keyed pairs to console
        for (Map.Entry<Integer, java.lang.constant.Constable> entry : s) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("\n--- Set ---");

        //Create set of colors
        Set<String> set = new TreeSet<>();

        set.add("Red");
        set.add("Red");
        set.add("Orange");
        set.add("Green");
        set.add("Blue");
        set.add("Purple");
        set.add("");
        set.add("Black");

        //Error handling for empty set
        boolean sans = set.isEmpty();
        if (sans){
            System.out.println("Error: Set is empty");
            System.exit(1);
        }

        else

        //Output set to console in alphabetical order with one duplicate and one null value
        for (String se : set) {
            System.out.print(se + " \n");
        }

        System.out.println("\n--- Generics ---");
        //Create linked list
        List<Cars> autos = new LinkedList<>();
        autos.add(new Cars("Ford      ", "Mustang GT 500"));
        autos.add(new Cars("Ford      ", "Mustang GT 350"));
        autos.add(new Cars("Ford      ", "Mustang Mach 1"));
        autos.add(new Cars("Ford      ", "Mustang Mach E"));
        autos.add(new Cars("Chevrolet ", "Corvette"));
        autos.add(new Cars("Chevrolet ", "Camaro SS"));
        autos.add(new Cars("Chevrolet ", "Silverado LTZ Z71"));

        //Error handling for empty linked list
        boolean gans = autos.isEmpty();
        if (gans){
            System.out.println("Error: List is empty");
            System.exit(1);
        }

        else

        //I forgot how to format the output into a table. I will have to research that
        for (Cars car : autos) {
            System.out.println(car);
        }
    }
}
