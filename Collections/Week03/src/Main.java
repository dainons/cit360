/**
 *  A Java program that gathers 2 numbers from the user
 *  then uses error handling and data validation
 *  before dividing the first number by the second.
 *  The output is then written to the console
 *
 *  Author: Dainon Snow
 *  CIT360
 *  1/20/2022
 */

import java.util.Scanner;

public class Main {

    //Main method describes purpose and calls the functions
    public static void main(String[] args) {

        System.out.println("\nThis program divides a first number by a second.\n");

        //Calls function to get 1st number and perform error handling and validation
        float num1 = getNum1();

        //Calls function to get 2nd number and perform error handling and validation
        float num2 = getNum2();

        //Calls function to compute the calculation with validation
        float answer = compute(num1, num2);

        //Write result to the console and end the program
        System.out.println("\n" + num1 + " divided by " + num2 + " equals " + answer);
        System.out.println("********  End of Program ********");

    }

    //Function to get first number
    static float getNum1() {

        //Define variables
        int in = 0;
        boolean test = false;

        //Loop to gather input from user and try error handling until correct
        while (!test) {
            String input;
            System.out.println("Enter the first number:");
            Scanner userInput = new Scanner(System.in);
            input = userInput.nextLine();

            //Try input for alpha characters
            try {
                float testNum;
                testNum = Float.parseFloat(input);
                System.out.println(testNum + " is valid.");
                //If true, return value
                return testNum;

            //Catch for data validation, repeat loop if true
            } catch (NumberFormatException iMe) {
                System.out.println("\nYou entered an alpha character: " + input);
                System.out.println("\nPlease try again. ");
            }

        //Return to satisfy float function syntax. Will never be true.
        }return in;
    }

    //Function to get second number
    static float getNum2() {

        //Define variables
        int in = 0;
        boolean test = false;

        //Loop to gather input from user and try error handling until correct
        while (!test) {
            String input;
            System.out.println("Enter the Second number:");
            Scanner userInput = new Scanner(System.in);
            input = userInput.nextLine();

            ////Try input for alpha characters
            try {
                float testNum;
                testNum = Float.parseFloat(input);
                //Function to test if denominator is zero
                testNum=isZero(testNum);
                System.out.println(testNum + " is valid.");
                //If true, return value
                return testNum;

            //Catch for data validation, repeat loop if true
            } catch (NumberFormatException iMe) {
                System.out.println("\nYou entered an alpha character: " + input);
                System.out.println("\nPlease try again. ");
            }

        //Return to satisfy float function syntax. Will never be true.
        }return in;
    }

    //Function to test if denominator is zero
    static float isZero(float input){

        //Define variables
        boolean test = false;
        if (input==0) {

            //Loop to prompt user to enter a new value
            while (input==0) {
                System.out.println("\nThe denominator cannot be zero. Try again. ");
                Scanner userInput = new Scanner(System.in);
                String newInput = userInput.nextLine();

                //Error handling and data validation for new value to prevent an alpha character
                while (!test) {

                    //Try value for float, if true break out of loop
                    try {
                        Float.parseFloat(newInput);
                        break;

                    //Catch to inform user to choose a numeric value
                    } catch (NumberFormatException iMe) {
                        System.out.println("\nYou entered an alpha character: " + newInput);
                        System.out.println("\nPlease try again. ");
                        Scanner userInput2 = new Scanner(System.in);
                        newInput = userInput2.nextLine();
                    }
                }

                //Repeat loop with new value
                input = Float.parseFloat(newInput);
            }
        }

        //If conditions are all true, return new value to 2nd number function
       return input;
    }

    //Compute result of division, Borrowed Error Handling ArithmeticException from Marvel Okafor
    static float compute(float num1, float num2) throws ArithmeticException {
        return num1/num2;
    }
}
