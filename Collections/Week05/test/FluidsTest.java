/**
 *  A Java program that simulates a car dealership
 *  delivery check list for fluids to make certain
 *  the car is ready for customer delivery.
 *
 *  It simulates service advisor input in setters
 *  and checks the result against an expected response.
 *
 *  The tests are performed using multiple JUnit
 *  assert methods
 *
 *  The last method intentionally returns an error.
 *
 *  Author: Dainon Snow
 *  CIT360
 *  2/4/2022
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FluidsTest {

    //Test to determine if the vehicle was fueled with the correct gasoline grade
    @Test
    void getGasolineType() {
        Fluids gas = new Fluids("", 0, "", "");
        //Simulated advisor input
        gas.setGasolineType("Premium 93 Octane");

        //Correct response should be Premium fuel
        try {
            assertEquals("Premium 93 Octane", gas.getGasolineType());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //Test to determine the amount of fuel in the tank
    @Test
    void getPercentFull() {
        Fluids gas = new Fluids("", 90, "", "");
        //Simulated gas gauge input
        gas.setPercentFull(85.55);

        //Fuel tank should be between 5% and 90% full for delivery
        try {
            assertTrue(gas.getPercentFull() > (5) && gas.getPercentFull() < (90));
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //Test to check that the coolant type is present, and not NULL
    @Test
    void getCoolantType() {
        Fluids coolant = new Fluids("", 0, "", "");
        //Simulated advisor input
        coolant.setCoolantType("GM DexCool");

        //Coolant should not be NULL. Throws error if forgotten.
        try {
            assertNotNull(coolant.getCoolantType());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //Test to check if coolant is topped off
    @Test
    void getTopOff() {
        Fluids coolant = new Fluids("", 100, "", "");
        //Simulated advisor input
        coolant.setPercentFull(100);

        //Coolant amount should not be less than 100%
        try {
            assertFalse(coolant.getPercentFull() < 100);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //Test to verify the VIN on the vehicle
    @Test
    void getVin() {

        Fluids vin = new Fluids("", 0, "", "");
        //Simulated advisor input for a 2017 Dodge Viper GT
        vin.setVin("1C3BDEDZ5HV731948");

        //If VIN does not match expected, throws error
        try {
            assertSame(vin.getVin(), "1C3BDEDZ5HV731948");
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //An intentional failed test to throw error
    @Test
    void getWrongGasolineType() {
        Fluids gas = new Fluids("", 0, "", "");
        //Lot boy puts the wrong fuel in this Dodge Viper
        gas.setGasolineType("Super Unleaded 88 Octane");

        //Premium fuel is expected, but Super Unleaded was dispensed
        try {
            assertEquals("Premium 93 Octane", gas.getGasolineType());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}