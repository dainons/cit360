/**Constructors, getters, and setters
 * Some code from Dr. Brian Fraser's YouTube channel
 * */

public class Fluids {
   private String gasolineType;
   private double percentFull;
   private String coolantType;
   private String vin;

   public Fluids(String gasolineType, double percentFull, String coolantType, String vin) {
       this.gasolineType = gasolineType;
       this.percentFull = percentFull;
       this.coolantType = coolantType;
       this.vin = vin;
   }
   public String getGasolineType() {
       return gasolineType;
   }

   public double getPercentFull() {
       return percentFull;
   }

   public String getCoolantType() {
       return coolantType;
   }

   public String getVin() {
       return vin;
   }

   public void setGasolineType (String gasolineType) {
       this.gasolineType = gasolineType;
   }

   public void setPercentFull (double percentFull) {
       this.percentFull = percentFull;
   }

    public void setCoolantType(String coolantType) {
        this.coolantType = coolantType;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
