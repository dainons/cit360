package Week04;

//Car data constructor taking four values
public class Car {
    private int year;
    private String make;
    private String model;
    private String color;

    public void setYear (int year) {
        this.year=year;
    }

    public void setMake (String make){
        this.make=make;
    }

    public void setModel (String model){
        this.model=model;
    }

    public void setColor (String color){
        this.color=color;
    }

    public int getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String toString() {
        return "Year: " + year + "\nMake: " + make + "\nModel: " + model + "\nColor: " + color;
    }
}
