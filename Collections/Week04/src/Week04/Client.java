package Week04;
import java.net.*;
import java.io.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
public class Client {

        // Method to get HTTP content from the HTTP Server. Code from Brother Tuckett.
        public static String getHttpContent(String string) {

            String content="";

            try {
                URL url = new URL(string);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();

                BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();

                String line = null;
                while ((line= reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                content = stringBuilder.toString();

            } catch (Exception e) {
                System.err.println(e.toString());
            }

            return content;
        }

        public static void main(String[] args) {

            System.out.println("\nIn JSON format: ");
            //Make call to the HTTP server, and print the JSON. Code from Marvel Okafor
            System.out.println(Client.getHttpContent("http://localhost:7200"));

            //Convert JSON to string in Jackson Library Pretty Printer Format. Code from kodejava.org.
            try {
            String json = (Client.getHttpContent("http://localhost:7200"));
            System.out.println("In Jackson Printer Pretty format: ");
            ObjectMapper mapper = new ObjectMapper();
            Object jsonObject = mapper.readValue(json, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
            System.out.println(prettyJson);

            }
            catch (Exception e){
                System.err.println(e.toString());
            }

            //Brother Tucket demo format
            Car car2 = JSONtoCar(Client.getHttpContent("http://localhost:7200"));
            System.out.println("\nIn Brother Tuckett Demo format: ");
            System.out.println(car2);

            System.out.println("\n***The Program completed successfully.***");
    }

        //Method from Brother Tuckett
        public static Car JSONtoCar(String s) {

            ObjectMapper mapper = new ObjectMapper();
            Car car = null;

            try {
                car = mapper.readValue(s, Car.class);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return car;
        }
 }


