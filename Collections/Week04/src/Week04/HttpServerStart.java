/**
 *  A Java program that makes an HTTP request to
 *  a Java server which returns a string in JSON
 *  format. The client then converts the JSON
 *  string to a printer friendly string format
 *  and prints the result to the console.
 *
 *  Author: Dainon Snow
 *  CIT360
 *  1/28/2022
 */

package Week04;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.net.*;
import java.io.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.nio.charset.StandardCharsets;


public class HttpServerStart {

    //Main method that starts the Java HTTP Server on port 7200. Code from logicbig.com tutorials.
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(7200), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(HttpServerStart::handleRequest);
        server.start();
    }

    //Method to handle the HTTP request from the client.
    private static void handleRequest(HttpExchange exchange) throws IOException {
        //Function to construct the car object
        String response = getData();

        //Get and send the HTTP response headers. Code from Antonio Lefiñir.
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        exchange.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);

        //Write the stream to the HTTP page. Code from logicbig.com tutorials.
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    //Method to construct the car data with the Car.java class.
    public static String getData() throws JsonProcessingException {

        Car car = new Car();
            car.setYear(2001);
            car.setMake("Chevrolet");
            car.setModel("Corvette");
            car.setColor("Nassau Blue");
        //Using the Jackson library, convert the object to a JSON string
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(car);

    return json;
    }
}












