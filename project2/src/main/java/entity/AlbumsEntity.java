package entity;

import javax.persistence.*;
import java.util.Comparator;

@Entity
@Table(name = "albums", schema = "dbo", catalog = "master")
public class AlbumsEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private int id;
    @Basic
    @Column(name = "Year")
    private Integer year;
    @Basic
    @Column(name = "Artist")
    private String artist;
    @Basic
    @Column(name = "Album")
    private String album;
    @Basic
    @Column(name = "Stars")
    private String stars;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlbumsEntity that = (AlbumsEntity) o;

        if (id != that.id) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (artist != null ? !artist.equals(that.artist) : that.artist != null) return false;
        if (album != null ? !album.equals(that.album) : that.album != null) return false;
        if (stars != null ? !stars.equals(that.stars) : that.stars != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (artist != null ? artist.hashCode() : 0);
        result = 31 * result + (album != null ? album.hashCode() : 0);
        result = 31 * result + (stars != null ? stars.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AlbumsEntity{" +
                "id=" + id +
                ", year=" + year +
                ", artist='" + artist + '\'' +
                ", album='" + album + '\'' +
                ", stars='" + stars + '\'' +
                '}';
    }

    //Comparator to organize the database info by year, then artist by alphabetical order
    static class sortByYear implements Comparator<AlbumsEntity> {
        public int compare( AlbumsEntity first, AlbumsEntity second) {

            return first.getYear() - second.getYear();
        }
    }

}
