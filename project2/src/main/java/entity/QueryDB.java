/**CIT360 Week14 Personal Project
 * Author: Dainon Snow
 * Revised 4/01/2022
 */


/** A Java Program that starts a servlet and a Tomcat Server.
 *  The program uses hibernate to connect to a SQL Server Express
 *  Database. The program then collects the data in the
 *  album database into a List collection. A comparator then sorts the
 *  album information by the year of release and then the artist by
 *  alphabetical order. The list is then displayed to the user. A
 *  JavaScript function is used to ask the user if they want to add to the
 *  database. If add is selected, the new info is collected from the
 *  user and the rows are written to the database. The process repeats
 *  until the user closes the HTML index page.
 *  */

package entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;

/** QueryDB implemented using a singleton pattern.
 *  Used to get vehicle data from the SQL Server database.
 *  This class code from Brother Tuckett.
 *  */
public class QueryDB {

    SessionFactory factory = null;
    Session session = null;

    private static QueryDB single_instance = null;

    private QueryDB()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static QueryDB getInstance()
    {
        if (single_instance == null) {
            single_instance = new QueryDB();
        }

        return single_instance;
    }

    /** Used to get albums from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<AlbumsEntity> getAlbums() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.AlbumsEntity";
            List<AlbumsEntity> cs = (List<AlbumsEntity>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}

