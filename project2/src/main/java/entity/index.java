package entity;

import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

// Some code from Brother Tuckett tutorial
@WebServlet(name = "index", urlPatterns = {"/index.html"})
public class index extends HttpServlet {

    //Method to start Tomcat and get the parameters from the index page
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        //HTML code from Marvel Okafor
        out.println("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <title>Album Catalog</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Chronological Inventory of Albums</h1>\n" +
                "\n" +
                "<! some form elements from codejava.net>\n" +
                "\n <table>" +
                "                    <thead>\n" +
                "                   <tr style=\"text-align: left; \"> \n" +
                "                  <th> Year</th>\n" +
                "                <th>Artist Name</th>\n" +
                "                        <th>Album</th>\n" +
                "                        <th>Stars</th>\n" +
                "                    </tr>\n" +
                "                    </thead>\n" +
                "                   <tbody>\n");


        //QUERY DB
        QueryDB con = QueryDB.getInstance();

        //Create list of albums in database
        List<AlbumsEntity> artistName = con.getAlbums();
        Collections.sort(artistName, new AlbumsEntity.sortByYear());

        //Write database info to HTML page in table format
        for (int i = 0; i< artistName.size(); i++) {
            out.println("<tr>" +
                    "<td>" + artistName.get(i).getYear() + "</td>" +
                    "<td>" + artistName.get(i).getArtist() + "</td>" +
                    "<td>" + artistName.get(i).getAlbum() + "</td>" +
                    "<td>" + artistName.get(i).getStars() + "</td>" +
                    "</tr>");
        }

        //HTML to get album data from user and initiate data validation
        out.println("</tbody>" +
                "</table>" +
                "" +
                "<form style=\"display:none;\" id=\"addAlbum\" action=\"cube-servlet\" method=\"post\">\n" +
                "  Year: <label>\n" +
                "  <input type=\"number\" size=\"1\" min=\"1900\" max=\"2022\" step=\"1\" name=\"Year\" placeholder=\"YYYY\" required/>\n" +
                "</label>\n" +
                "  &nbsp;&nbsp;\n" +
                "  Artist: <label>\n" +
                "  <input size=\"15\" name=\"Artist\" placeholder=\"ex. Journey\" required/>\n" +
                "</label>\n" +
                "  &nbsp;&nbsp;\n" +
                "  Album: <label>\n" +
                "  <input size=\"15\" name=\"Album\" placeholder=\"ex. Captured\" required/>\n" +
                "</label>\n" +
                "  &nbsp;&nbsp;\n" +
                "\n" +
                "  <! menu pull down from W3schools.com>\n" +
                "\n" +
                "  <label for=\"uom\">Stars:</label>\n" +
                "  <select name=\"uom\" id=\"uom\">\n" +
                "    <option value=\"one\">1</option>\n" +
                "    <option value=\"two\">2</option>\n" +
                "    <option value=\"three\">3</option>\n" +
                "    <option value=\"four\">4</option>\n" +
                "    <option value=\"five\">5</option>\n" +
                "  </select>\n" +
                "\n" +
                "  <br/><br/>\n" +
                "  <input style=\"display:none;\" id=\"submitAlbumButton\" type=\"submit\" value=\"Enter\" />\n" +
                "<button id=\"closeButton\" style=\"display:none;\" onclick=\"closeForm()\">Close</button>" +
                "</form>\n" +
                "<button id=\"openForm\" onclick=\"openForm()\">Add Album</button>\n" +
                "<script src=\"js/basic.js\"></script>" +
                "</body>\n" +
                "</html>");
    }
}