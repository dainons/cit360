package entity;

import org.hibernate.Session;

import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;


// Some code from Brother Tuckett Week 07 tutorial
@WebServlet(name = "cubeServlet", urlPatterns = {"/cube-servlet"}

)
public class HelloServlet extends HttpServlet {
    int year;
    String stars = null;
    String inputYear;
    String inputArtist;
    String inputAlbum;
    String uom;

    //Method to start Tomcat and get the parameters from the index page
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

           inputYear = request.getParameter("Year");

           //Make sure the year is an integer
           try {
               year = Integer.parseInt(inputYear);
           } catch (Exception e) {
               e.printStackTrace();
           }


           inputArtist = request.getParameter("Artist");
            //Check length of string to match database VARCHAR
           try {
            if (inputArtist.length() <= 50) {}

                } catch (Exception e){
            System.out.println ("Exceeds Database string length: " + e);
            }

            inputAlbum = request.getParameter("Album");
           //Check length of Album to match database VARCHAR
             try {
                if (inputAlbum.length() <= 50) {}

                } catch (Exception e){
            System.out.println ("Exceeds Database string length: " + e);
            }

            uom = request.getParameter("uom");
            //Check number of stars for null
            try {
                if(uom != null && !uom.isEmpty()) {}

                } catch (Exception e){
            System.out.println ("Field cannot be empty: " + e);
            }

        //Switch to convert pull down uom to asterisks.
        switch (uom) {

            case "one":
                stars = "*";
                break;
            case "two":
                stars = "**";
                break;
            case "three":
                stars = "***";
                break;
            case "four":
                stars = "****";
                break;
            case "five":
                stars = "*****";
                break;
        }


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        AlbumsEntity artistName2 = new AlbumsEntity();

        //Construct database transaction for insert
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        artistName2.setArtist(inputArtist);
        artistName2.setAlbum(inputAlbum);
        artistName2.setYear(year);
        artistName2.setStars(stars);

        //save the album info in the database
        session.save(artistName2);


        //commit the transaction
        session.getTransaction().commit();

        //Close the session
        session.close();

        out.println(    "<script>" +
                "window.location.href = \"http://localhost:8080/db2/index.html\""+
                "</script>" );
    }

    //Destroy the servlet
    public void destroy() {
    }
}

