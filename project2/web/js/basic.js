

function openForm() {
    //hide the add album button and the form
    document.getElementById("openForm").style.display = "none";

    //display the Enter,form and Close button
    document.getElementById("submitAlbumButton").style.display = "inline-block";
    document.getElementById("closeButton").style.display = "inline-block";
    document.getElementById("addAlbum").style.display = "block";

}

function closeForm() { //do the opposite of openForm
    //show the add album button
    document.getElementById("openForm").style.display = "block";

    //hide the Enter and Close button
    document.getElementById("submitAlbumButton").style.display = "none";
    document.getElementById("closeButton").style.display = "none";
    document.getElementById("addAlbum").style.display = "none";
}